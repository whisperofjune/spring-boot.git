package com.xiaojie.score.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 配置积分队列和交换机
 * @date 2021/10/10 22:09
 */
@Component
public class MqConfig {
    //定义队列
    private static  final  String SCORE_QUEUE="xiaojie_score_queue";
    //定义交换机
    private static  final  String SCORE_EXCHANGE="xiaojie_score_exchaneg";
    //定义路由键
    private static  final  String SCORE_ROUTINNGKEY="score.add";

    @Bean
    public Queue scoreQueue(){
        return  new Queue(SCORE_QUEUE);
    }
    @Bean
    public DirectExchange scoreDirectExchange(){
        return new DirectExchange(SCORE_EXCHANGE);
    }
    @Bean
    public Binding scoreBinding(Queue scoreQueue,DirectExchange scoreDirectExchange ){
        return BindingBuilder.bind(scoreQueue).to(scoreDirectExchange).with(SCORE_ROUTINNGKEY);
    }
}
