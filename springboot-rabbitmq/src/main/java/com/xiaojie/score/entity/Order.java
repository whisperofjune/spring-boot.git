package com.xiaojie.score.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/8 22:04
 */
@Data
public class Order implements Serializable {
    private Long id;
    private String orderId;
    private String orderName;
    //订单状态，0未支付，1已支付，2-已过期
    private Integer status;
    //创建时间
    private Date createTime;
    //总钱数
    private Double payMoney;
}
