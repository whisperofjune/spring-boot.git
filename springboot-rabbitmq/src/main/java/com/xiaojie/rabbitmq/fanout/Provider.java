package com.xiaojie.rabbitmq.fanout;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.xiaojie.rabbitmq.MyConnection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author xiaojie
 * @version 1.0
 * @description: fanout模式生产者
 * @date 2021/9/24 23:45
 */
public class Provider {
    public static final  String EXCHANGE="my_fanout_exchange";


    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        //创建连接
        Connection connection = MyConnection.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
//        channel.confirmSelect();//开启确认模式，确认消息已经被mq持久化到硬盘上
//        channel.basicQos(1);//每次发送一条消息，并且收到消费者的ack之后才会发送第二条
        //生产者绑定交换机 参数1 交换机的名称。2，交换机的类型（扇形交换机）
        channel.exchangeDeclare(EXCHANGE, "fanout");
        //创建消息
        String msg="fanout交换机消息。。。。";
        //发送消息
        channel.basicPublish(EXCHANGE, "", null, msg.getBytes(StandardCharsets.UTF_8));
//        if(channel.waitForConfirms()){
            //确认消息已经持久化到硬盘
            System.out.println("消息发送成功。。。。。。");
//        }
        //关闭通道，关闭连接
        channel.close();
        connection.close();
    }
}
