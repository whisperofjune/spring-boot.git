package com.xiaojie.rabbitmq.p2p;

import com.rabbitmq.client.*;
import com.xiaojie.rabbitmq.MyConnection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/9/24 23:15
 */
public class PConsumer {
    private static final String QUEUE_NAME = "myqueue";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 1.创建我们的连接
        Connection connection = MyConnection.getConnection();
        // 2.创建我们通道
        Channel channel = connection.createChannel();
        DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String msg = new String(body, "UTF-8");
                System.out.println("消费消息msg:" + msg);
            }
        };
        // 3.创建我们的监听的消息
        channel.basicConsume(QUEUE_NAME, true, defaultConsumer);

    }
}
