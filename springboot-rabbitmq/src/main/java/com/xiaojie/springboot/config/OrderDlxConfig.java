package com.xiaojie.springboot.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
/**
 * @author xiaojie
 * @version 1.0
 * @description:解决订单超时未支付问题，绑定订单死信队列
 * @date 2021/10/8 23:12
 */
@Component
public class OrderDlxConfig {

    @Value(value="${xiaojie.order.queue}")
    private String orderQueue; //订单队列
    @Value(value="${xiaojie.order.exchange}")
    private String orderExchange;//订单队列
    @Value(value="${xiaojie.dlx.queue}")
    private String orderDeadQueue;//订单死信队列
    @Value(value="${xiaojie.dlx.exchange}")
    private String orderDeadExChange;//订单死信交换机
    @Value(value="${xiaojie.order.routingKey}")
    private String orderRoutingKey;//订单路由键
    @Value(value="${xiaojie.dlx.routingKey}")
    private String orderDeadRoutingKey;//死信队列路由键

    @Bean
    public Queue orderQueue(){
        Map<String, Object> args = new HashMap<>(2);
        // 绑定我们的死信交换机
        args.put("x-dead-letter-exchange", orderDeadExChange);
        // 绑定我们的路由key
        args.put("x-dead-letter-routing-key", orderDeadRoutingKey);
        return new Queue(orderQueue, true, false, false, args);
    }
    @Bean
    public Queue orderDeadQueue(){
        return new Queue(orderDeadQueue);
    }
    //绑定交换机
    @Bean
    public DirectExchange orderExchange(){
        return new DirectExchange(orderExchange);
    }
    @Bean
    public DirectExchange orderDeadExchange(){
        return new DirectExchange(orderDeadExChange);
    }

    //绑定路由键
    @Bean
    public Binding orderBindingExchange(Queue orderQueue, DirectExchange orderExchange) {
        return BindingBuilder.bind(orderQueue).to(orderExchange).with(orderRoutingKey);
    }
    //绑定死信队列到死信交换机
    @Bean
    public Binding deadBindingExchange(Queue orderDeadQueue,  DirectExchange orderDeadExchange) {
        return BindingBuilder.bind(orderDeadQueue).to(orderDeadExchange).with(orderDeadRoutingKey);
    }
}
