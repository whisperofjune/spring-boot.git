package com.xiaojie.springboot.controller;

import com.xiaojie.springboot.entity.Order;
import com.xiaojie.springboot.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 订单Controller
 * @date 2021/10/8 23:10
 */
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;
    @RequestMapping("/saveOrder")
    public String saveOrder(){
        Order order=new Order();
        return orderService.saveOrder(order);
    }
}
