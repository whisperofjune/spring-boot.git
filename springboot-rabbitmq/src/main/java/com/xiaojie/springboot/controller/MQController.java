package com.xiaojie.springboot.controller;

import com.xiaojie.springboot.provider.DLXProvider;
import com.xiaojie.springboot.provider.MsgProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 测试发送消息
 * @date 2021/9/25 23:03
 */
@RestController
public class MQController {
    @Autowired
    private MsgProvider msgProvider;
    @Autowired
    private DLXProvider dlxProvider;
    @GetMapping("/send")
    public String send(Integer type){
        String msg="测试生产者生产消息。。。。。。";
       return  msgProvider.sendMSg(type,msg);
    }
    /**
     * @description: 模拟死信队列
     * @param:
     * @return: java.lang.String
     * @author xiaojie
     * @date: 2021/10/8 21:43
     */
    @GetMapping("/sendDlx")
    public String sendDlx(){
        return  dlxProvider.sendDlxMsg();
    }

}
