package com.xiaojie.springboot.service;

import com.xiaojie.springboot.entity.Order;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/8 22:16
 */
public interface OrderService {
    /**
     * @description: 创建订单
     * @param:
     * @param: order
     * @return: java.lang.Integer
     * @author xiaojie
     * @date: 2021/10/8 22:24
     */

    String saveOrder(Order order);
    /**
     * @description: 根据订单编号查询订单
     * @param:
     * @param: orderId
     * @return: com.xiaojie.springboot.entity.Order
     * @author xiaojie
     * @date: 2021/10/8 22:25
     */
    Order getByOrderId(String orderId);
    /**
     * @description: 修改订单状态未支付
     * @param:
     * @param: orderId
     * @return: java.lang.Integer
     * @author xiaojie
     * @date: 2021/10/8 22:26
     */
    Integer updateOrderStatus(String orderId);
}
