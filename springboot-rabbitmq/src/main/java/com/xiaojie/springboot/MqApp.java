package com.xiaojie.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/9/25 23:06
 */
@SpringBootApplication
@EnableAsync
@MapperScan("com.xiaojie.springboot.mapper")
public class MqApp {
    public static void main(String[] args) {
        SpringApplication.run(MqApp.class);
    }
}
