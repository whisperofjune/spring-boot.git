package com.xiaojie.job.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.17
 */
@RestController
public class IndexController {
    @GetMapping("/index")
    public String index(){
        return "success";
    }

}
