package com.xiaojie.job.myenum;

/**
 * @Description: 分片集群枚举
 * @author: yan
 * @date: 2021.11.17
 */
public enum ShardEnum {
    ONE(1,"firstShard"),
    TWO(2,"secondShard"),
    THREE(3,"third") ;
    private int ShardNum;
    private String ShardText;
    ShardEnum(int shardNum, String shardText) {
        ShardNum = shardNum;
        ShardText = shardText;
    }

    public int getShardNum() {
        return ShardNum;
    }

    public void setShardNum(int shardNum) {
        ShardNum = shardNum;
    }
    public String getShardText() {
        return ShardText;
    }

    public void setShardText(String shardText) {
        ShardText = shardText;
    }
}
