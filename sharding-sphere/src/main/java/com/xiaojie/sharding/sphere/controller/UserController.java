package com.xiaojie.sharding.sphere.controller;

import com.xiaojie.sharding.sphere.entity.User;
import com.xiaojie.sharding.sphere.mapper.UserMapper;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/3/6 1:10
 */
@RestController
public class UserController {
    @Autowired
    private UserMapper userMapper;

    @GetMapping("/add")
    public String add(){
        for (int i=1;i<100;i++){
            User user=new User();
            user.setName("tom"+i);
            user.setAge(RandomUtils.nextInt(18,40));
            user.setGender(1);
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());
            userMapper.insert(user);
        }
        return "success";
    }
    @GetMapping("/getAll")
    public List<User> getAll(){
        List<User> users = userMapper.selectAll();
        return users;
    }
    @GetMapping("/getById")
    public User getById(Long id){
        User user = userMapper.selectById(id);
        return user;
    }

    @GetMapping("/getByAge")
    public List<User> getByAge(){
        List<User> users = userMapper.selectByAge();
        return users;
    }
}
