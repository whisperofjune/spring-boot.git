package com.xiaojie.sharding.sphere.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:
 * @author: yan
 * @date: 2022.03.03
 */
@Data
public class User implements Serializable {
    private Long id;
    private String name;
    private Integer age;
    private Integer gender;
    private Date createTime;
    private Date updateTime;
}
