package com.xiaojie.sharding.sphere.mapper;


import com.xiaojie.sharding.sphere.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description:
 * @author: yan
 * @date: 2022.03.03
 */
public interface UserMapper {

    @Select("select * from tb_user limit 0,10")
    List<User> selectAll();

    @Select("select * from tb_user where name=#{name}")
    List<User> selectByName(String name);

    @Insert("insert INTO tb_user(name,age,gender,create_time,update_time) VALUES(#{name},#{age},#{gender},now(),now())")
//    @Options(useGeneratedKeys=true,keyProperty="id", keyColumn="id")
    Integer insert(User user);
    @Select("select * from tb_user where id=#{id}")
    User selectById(Long id);

    @Select("select * from tb_user where age>20 and  age<=30")
    List<User> selectByAge();

}
