package com.xiaojie.sharding.sphere;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @author: yan
 * @date: 2022.03.08
 */
@SpringBootApplication
@MapperScan("com.xiaojie.sharding.sphere.mapper")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }
}
