package com.xiaojie.mapper;

import com.xiaojie.entity.Dispatch;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/11 22:54
 */
@Repository
public interface DispatchMapper {

    @Insert("insert into tb_dispatch(orderId,userId,riderId,status, createTime,sendTime) values(#{orderId},#{userId},#{riderId},0,now(),#{sendTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer saveDispatch(Dispatch dispatch);
}
