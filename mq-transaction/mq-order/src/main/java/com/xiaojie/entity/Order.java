package com.xiaojie.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: 订单实体类
 * @author xiaojie
 * @date 2021/10/11 21:53
 * @version 1.0
 */
@Data
public class Order implements Serializable {
    private Long id;
    private String orderId;
    private String orderName;
    //订单状态，0未支付，1已支付，2-已过期
    private Integer status;
    //创建时间
    private Date createTime;
    //总钱数
    private Double payMoney;
}