package com.xiaojie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.10.12
 */
@SpringBootApplication
@MapperScan("com.xiaojie.mapper")
public class MQOrderApp {
    public static void main(String[] args) {
        SpringApplication.run(MQOrderApp.class);
    }
}
