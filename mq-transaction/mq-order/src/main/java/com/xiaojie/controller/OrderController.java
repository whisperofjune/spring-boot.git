package com.xiaojie.controller;

import com.xiaojie.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 外卖下单完成，
 * @date 2021/10/11 22:06
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/addOrder")
    public String addOrder(){
        return orderService.saveOrder();
    }
}
