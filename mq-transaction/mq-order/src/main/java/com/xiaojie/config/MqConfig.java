package com.xiaojie.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: mq配置 ,补单队列和派单队列绑定到同一个交换机上，订阅发布fanout交换机
 * @date 2021/10/11 21:56
 */
@Component
public class MqConfig {

    //定义补单队列
    private static final String ORDER_QUEUE="xiaojie_order_queue";
    //定义派单队列
    private static final String DISPATCH_QUEUE="dispatch_order_queue";
    //定义交换机
    private static final String XIAOJIE_ORDER_EXCHANGE="xiaojie_order_exchange";

    //订单队列
    @Bean
    public Queue orderQueue(){
        return new Queue(ORDER_QUEUE);
    }
    //派单队列
    @Bean
    public Queue dispatchQueue(){
        return new Queue(DISPATCH_QUEUE);
    }
    //交换机
    @Bean
    public FanoutExchange fanoutExchange(){
        return new FanoutExchange(XIAOJIE_ORDER_EXCHANGE);
    }
    //绑定订单队列到交换机
    @Bean
    public Binding orderBinding(Queue orderQueue,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(orderQueue).to(fanoutExchange);
    }
    //绑定派单队列到交换机
    @Bean
    public Binding dispatchBinding(Queue dispatchQueue,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(dispatchQueue).to(fanoutExchange);
    }
}
