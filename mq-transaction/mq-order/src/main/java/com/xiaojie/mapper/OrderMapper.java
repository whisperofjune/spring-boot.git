package com.xiaojie.mapper;

import com.xiaojie.entity.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 订单类
 * @date 2021/10/8 22:15
 */
public interface OrderMapper {
    @Insert("insert tb_order values (null,#{orderId},#{orderName},0,now(),#{payMoney})")
    int addOrder(Order order);

    @Select("SELECT * from tb_order where orderId=#{orderId} ")
    Order getOrder(String orderId);

    @Update("update tb_order set status=2 where orderId=#{orderId}")
    int updateOrder(String orderId);
}