package com.xiaojie.es.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/*
 *
 * @param null
 * @todo swagger配置类
 *   访问路径 http://localhost:8080/swagger-ui/index.html#/
 * @author yan
 * @date 2021/11/30 14:59
 * @return
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        // 添加全局参数token令牌
        List<RequestParameter> requestParameter = new ArrayList<>();
        requestParameter.add(new RequestParameterBuilder()
                .name("token")
                .description("令牌")
                .required(false)
                .in(ParameterType.HEADER)
                .build());
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build().globalRequestParameters(requestParameter);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口开发文档")
                .description("Snail ........")
                .version("1.0")
                .contact(new Contact("snail", "https://blog.csdn.net/weixin_39555954",
                        "whisper_snail@163.com"))
                .build();
    }
}
