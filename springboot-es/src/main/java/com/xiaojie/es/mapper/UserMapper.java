package com.xiaojie.es.mapper;

import com.xiaojie.es.entity.User;
import org.apache.ibatis.annotations.Insert;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.30
 */
public interface UserMapper {

    @Insert("INSERT INTO tb_user (name,age,position,address,create_time,update_time,delete_flag) " +
            "VALUES(#{name},#{age},#{position},#{address},now(),now(),0)")
     Integer add(User user);
}
