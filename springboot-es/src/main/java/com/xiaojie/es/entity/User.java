package com.xiaojie.es.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.30
 */
@Data
@ApiModel("用户")
@Document(indexName = "user",createIndex = true)
public class User {

    @ApiModelProperty(value="用户id",required = true)
    @Field(type = FieldType.Integer)
    private Integer id ;
    @ApiModelProperty("用户名称")
    @Field(type = FieldType.Text)
    private String name;
    @ApiModelProperty("用户年龄")
    @Field(type = FieldType.Integer)
    private Integer age;
    @Field(type = FieldType.Date)
    private Date createTime;
    @Field(type = FieldType.Date)
    private Date updateTime;
    @ApiModelProperty("删除标识1-删除，0-未删除")
    @Field(type = FieldType.Integer)
    private Integer deleteFlag;
    @Field(type = FieldType.Text)
    @ApiModelProperty("用户职务")
    private String position;
    @Field(type = FieldType.Text)
    @ApiModelProperty("用户住址")
    private String address;
}
