package com.xiaojie.mongodb.service;

import com.xiaojie.mongodb.model.TestUser;

import java.util.List;

/**
 * @Author: yan
 * @CreateTime: 2023-01-03  13:41
 * @Description: TODO
 * @Version: 1.0
 */
public interface TestUserService {

    List<TestUser> listByPage(int page, int pageSize);

    void saveTestUser();

    void updateTestUser(Long userId);

    void removeByUserId(Long userId);

}
