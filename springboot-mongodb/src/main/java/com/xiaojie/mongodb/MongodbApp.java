package com.xiaojie.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2023/1/6 0:05
 */
@SpringBootApplication
public class MongodbApp {

    public static void main(String[] args) {
        SpringApplication.run(MongodbApp.class, args);
    }
}
