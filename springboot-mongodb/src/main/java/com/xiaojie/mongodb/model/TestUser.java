package com.xiaojie.mongodb.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author yan
 * @since 2022-10-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = "test_user")
@Builder
@Accessors(chain = true)
public class TestUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @Id
    private Long id;

    /**
     * 用户名字全拼，如有重名，后面追加数字
     */
    private String pin;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 用户真实姓名
     */
    private String trueName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别0-男，1-女
     */
    private Integer gender;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 手机
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String head;

    /**
     * 部门id
     */
    private Long deptId;

    /**
     * 0-启用，1-禁用（默认0）
     */
    private Integer status;

    /**
     * 0-未删除；1-已删除，默认0
     */
    private Integer isDelete;

    /**
     * 0-在职，1-离职，2-入职中
     */
    private Integer postStatus;

    /**
     * 创建人，存pin
     */
    private String created;

    private String postCode;

    /**
     * 修改人
     */
    private String updated;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;


}
