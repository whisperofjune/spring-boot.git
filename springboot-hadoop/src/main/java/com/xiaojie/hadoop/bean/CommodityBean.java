package com.xiaojie.hadoop.bean;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 20:12
 */
public class CommodityBean implements WritableComparable<CommodityBean> {

    private String commodityId; //商品id
    private String commodityName; //商品名称
    private String picture; //商品图片
    private String price; //商品价格
    private String userId;
    private Long count; //计数


//    public CommodityBean(Long commodityId, String commodityName, String picture, BigDecimal price, Long userId, Long count) {
//        this.commodityId = commodityId;
//        this.commodityName = commodityName;
//        this.picture = picture;
//        this.price = price;
//        this.userId = userId;
//        this.count = count;
//    }

    public CommodityBean() {
    }

    public String getCommodityId() {
        return commodityId;
    }

    public void setCommodityId(String commodityId) {
        this.commodityId = commodityId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public int compareTo(CommodityBean o) {
        return commodityId.compareTo(o.commodityId);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(commodityId);
        out.writeUTF(commodityName);
        out.writeUTF(picture);
        out.writeLong(count);
        out.writeUTF(userId);
        out.writeUTF(price.toString());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.commodityId = in.readUTF();
        this.commodityName = in.readUTF();
        this.picture = in.readUTF();
        this.count = in.readLong();
        this.userId = in.readUTF();
        this.price = in.readUTF();
    }
}
