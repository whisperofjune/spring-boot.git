package com.xiaojie.hadoop.mapreduce.partitioner;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 自定义分区
 * @date 2024/12/29 15:52
 */
public class ProvincePartitioner extends Partitioner<Text, FlowBean> {

    /**
     * @param text          键值
     * @param flowBean      值
     * @param numPartitions 返回的分区数
     * @description: 分区逻辑, 手机号136、137、138、139开头都分别放到一个独立的4个文件中，其他开头的放到一个文件中
     * @return: int
     * @author 熟透的蜗牛
     * @date: 2024/12/29 15:54
     */
    @Override
    public int getPartition(Text text, FlowBean flowBean, int numPartitions) {
        int partition;
        if (StringUtils.isNotBlank(text.toString())) {
            if (text.toString().startsWith("136")) {
                partition = 0;
            } else if (text.toString().startsWith("137")) {
                partition = 1;
            } else if (text.toString().startsWith("138")) {
                partition = 2;
            } else if (text.toString().startsWith("139")) {
                partition = 3;
            } else {
                partition = 4;
            }
        } else {
            partition = 4;
        }
        return partition;
    }
}
