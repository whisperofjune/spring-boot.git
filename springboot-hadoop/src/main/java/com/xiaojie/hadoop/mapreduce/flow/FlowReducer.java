package com.xiaojie.hadoop.mapreduce.flow;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 定义流量输出的reduce
 * @date 2024/12/27 10:46
 */
public class FlowReducer extends Reducer<Text, FlowBean, Text, FlowBean> {

    private FlowBean finalOutV = new FlowBean();

    @Override
    protected void reduce(Text key, Iterable<FlowBean> values, Reducer<Text, FlowBean, Text, FlowBean>.Context context) throws IOException, InterruptedException {

        long totalUp = 0;
        long totalDown = 0;
        //遍历values,将其中的上行流量,下行流量分别累加
        for (FlowBean bean : values) {
            totalUp += bean.getUpFlow();
            totalUp += bean.getDownFlow();
        }
        //封装输出结果
        finalOutV.setUpFlow(totalUp);
        finalOutV.setDownFlow(totalDown);
        finalOutV.setSumFlow();
        //输出结果
        context.write(key, finalOutV);

    }
}
