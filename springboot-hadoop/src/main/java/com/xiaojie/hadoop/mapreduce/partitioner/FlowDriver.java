package com.xiaojie.hadoop.mapreduce.partitioner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 驱动
 * @date 2024/12/27 10:55
 */
public class FlowDriver {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {

        //获取job对象
        Configuration configuration = new Configuration();
        Job job = Job.getInstance(configuration);
        //设置jar
        job.setJarByClass(FlowDriver.class);

        //设置manpper 和reducer
        job.setMapperClass(FlowBeanMapper.class);
        job.setReducerClass(FlowReducer.class);

        //设置map输出kv类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowBean.class);

        //设置最终输出结果kv
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowBean.class);

        //设施任务数 ,这里设置的要和分区个数一致，如果任务数>分区数则输出文件会有多个为空的文件，如果任务数>1并且<分区数，会有数据无法处理发生异常，
        // 如果任务数为1 ，只会产生一个文件，分区号必须从0开始，逐渐累加
        job.setNumReduceTasks(5);

        //指定自定义分区类
        job.setPartitionerClass(ProvincePartitioner.class);
        //设置输入输出路径
        FileInputFormat.setInputPaths(job, new Path("d://hadoop//phone.txt"));
        FileOutputFormat.setOutputPath(job, new Path("d://hadoop//phone33"));

        //提交任务
        boolean result = job.waitForCompletion(true);
        System.exit(result ? 0 : 1);

    }
}
