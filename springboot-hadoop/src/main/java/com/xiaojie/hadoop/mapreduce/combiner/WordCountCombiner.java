package com.xiaojie.hadoop.mapreduce.combiner;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2024/12/29 18:50
 */
public class WordCountCombiner extends Reducer<Text, IntWritable, Text, IntWritable> {

   IntWritable outV= new IntWritable(0);
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
       int sum = 0;
        for (IntWritable val : values) {
            sum+=val.get();
        }
        outV.set(sum);
        context.write(key, outV);
    }
}
