package com.xiaojie.hadoop.mapreduce.combine;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2024/12/27 9:00
 */
public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    Text kOut = new Text();
    IntWritable vOut = new IntWritable(1);

    /**
     * @param key     偏移量
     * @param value   文本值
     * @param context 上下文
     * @description:
     * @return: void
     * @author 熟透的蜗牛
     * @date: 2024/12/27 9:01
     */
    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, IntWritable>.Context context) throws IOException, InterruptedException {
//        hello world
//        hello mapreduce
//        hello haddop
//        hadoop
//        java
//        mysql
//        mysql orcale
        /**
         这里输出的结果为（hello,1)(world,1)(hello,1) (mapreduce,1)(hello,1)......
         */


        //获取一行,输入的内容
        String line = value.toString();
        //分隔
        String[] words = line.split(" ");
        for (String word : words) {
            kOut.set(word);
            //kout 即为单词 vout 单词出现的次数
            context.write(kOut, vOut);
        }

    }
}
