package com.xiaojie.hadoop.mapreduce.combiner;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: reduce把map的输出当作输入
 * @date 2024/12/27 9:17
 */
public class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    int sum;
    IntWritable v = new IntWritable();

    /**
     * @param key     map 输出的key kOut
     * @param values  map输出的value Vout
     * @param context
     * @description:
     * @return: void
     * @author 熟透的蜗牛
     * @date: 2024/12/27 9:22
     */
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
        //累加求和，合并map传递过来的值
        sum = 0;
        for (IntWritable val : values) {
            sum += val.get();
        }
        //输出结果
        v.set(sum);
        context.write(key, v);
    }
}
