package com.xiaojie.hadoop.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 10:15
 */
public class ShaUtil {

    public static String getSha1(String str) {
        try {
            // 生成SHA-1哈希值
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] digest = md.digest(str.getBytes(StandardCharsets.UTF_8));
            // 将字节数组转换为十六进制字符串
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) sb.append('0');
                sb.append(hex);
            }
            String actualSHA1Hash = sb.toString();
            return actualSHA1Hash;
        } catch (
                NoSuchAlgorithmException e) {
            System.err.println("SHA-1算法不可用：" + e.getMessage());
        }
        return "123";
    }

}
