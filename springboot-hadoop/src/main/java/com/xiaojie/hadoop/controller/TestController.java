package com.xiaojie.hadoop.controller;

import com.xiaojie.hadoop.utils.HdfsClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.URISyntaxException;

@RestController
public class TestController {
    @Autowired
    private HdfsClientUtil hdfsClientUtil;

    @GetMapping("/mkDir")
    public String mkDir() throws URISyntaxException, IOException, InterruptedException {
        hdfsClientUtil.mkDirs();
        return "ok";
    }

    @GetMapping("/put")
    public String put() throws URISyntaxException, IOException, InterruptedException {
        hdfsClientUtil.putFile();
        return "ok";
    }
    @GetMapping("/download")
    public String download() throws URISyntaxException, IOException, InterruptedException {
        hdfsClientUtil.downloadFile();
        return "ok";
    }

    @GetMapping("/test")
    public String test() throws URISyntaxException, IOException, InterruptedException {
        //重命名
        // hdfsClientUtil.renameFile();


        //删除
        //hdfsClientUtil.deleteFile();

        //列出文件信息
        //hdfsClientUtil.listFiles();

        //判断是文件还是目录
        hdfsClientUtil.isFile();

        return "ok";
    }
}
