package com.xiaojie.hadoop.controller;

import com.xiaojie.hadoop.entity.Commodity;
import com.xiaojie.hadoop.entity.CommodityLogBean;
import com.xiaojie.hadoop.dao.CommodityDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 15:26
 */
@RestController
public class CommodityController {

    @Autowired
    private CommodityDao commodityDao;


    @GetMapping("/getDetailById")
    public Commodity getDetailById() {

        //省去mq这里模拟生成数据
        for (int i = 0; i < 500; i++) {
            CommodityLogBean commodityLogBean = new CommodityLogBean();
            String rowKey = StringUtils.reverse(new Date().getTime() + "");
            commodityLogBean.setRowId(rowKey);
            commodityLogBean.setCommodityId("45896542");
            commodityLogBean.setName("女士带花连衣裙");
            commodityLogBean.setPrice("99.9");
            commodityLogBean.setPicture("test");
            commodityLogBean.setUserId(i + "1001");
            commodityLogBean.setCategoryId("10000");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            commodityLogBean.setClickTime(sdf.format(new Date()));
            commodityDao.save(commodityLogBean);
        }
        for (int i = 0; i < 100; i++) {
            CommodityLogBean commodityLogBean = new CommodityLogBean();
            String rowKey = StringUtils.reverse(new Date().getTime() + "");
            commodityLogBean.setRowId(rowKey);
            commodityLogBean.setCommodityId("1563521");
            commodityLogBean.setName("女士带花连衣裙2");
            commodityLogBean.setPrice("59.9");
            commodityLogBean.setPicture("test111");
            commodityLogBean.setUserId(i + "10001");
            commodityLogBean.setCategoryId("25696");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            commodityLogBean.setClickTime(sdf.format(new Date()));
            commodityDao.save(commodityLogBean);
        }
        for (int i = 0; i < 50; i++) {
            CommodityLogBean commodityLogBean = new CommodityLogBean();
            String rowKey = StringUtils.reverse(new Date().getTime() + "");
            commodityLogBean.setRowId(rowKey);
            commodityLogBean.setCommodityId("999874562");
            commodityLogBean.setName("女士针织衫");
            commodityLogBean.setPrice("109.9");
            commodityLogBean.setPicture("test222");
            commodityLogBean.setUserId(i + "100001");
            commodityLogBean.setCategoryId("2222");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            commodityLogBean.setClickTime(sdf.format(new Date()));
            commodityDao.save(commodityLogBean);
        }
        for (int i = 0; i < 10; i++) {
            CommodityLogBean commodityLogBean = new CommodityLogBean();
            String rowKey = StringUtils.reverse(new Date().getTime() + "");
            commodityLogBean.setRowId(rowKey);
            commodityLogBean.setCommodityId("100045231");
            commodityLogBean.setName("男士羽绒服");
            commodityLogBean.setPrice("9.9");
            commodityLogBean.setPicture("test222322");
            commodityLogBean.setUserId(i + "100001");
            commodityLogBean.setCategoryId("333");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            commodityLogBean.setClickTime(sdf.format(new Date()));
            commodityDao.save(commodityLogBean);
        }

        return Commodity.builder().id(1111111l).name("男士羽绒服").price(new BigDecimal("9.9"))
                .categoryId(333l).picture("111111").description("抗寒抗风杠杠滴").build();


    }

    @GetMapping("/list")
    public List<CommodityLogBean> list() {
        return commodityDao.queryAll();
    }

}
