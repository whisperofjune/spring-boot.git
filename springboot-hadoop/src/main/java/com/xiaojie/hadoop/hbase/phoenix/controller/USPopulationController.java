package com.xiaojie.hadoop.hbase.phoenix.controller;

import com.xiaojie.hadoop.hbase.phoenix.bean.USPopulation;
import com.xiaojie.hadoop.hbase.phoenix.dao.PopulationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/4 1:42
 */
@RestController
public class USPopulationController {

    @Autowired
    private PopulationDao populationDao;

    @RequestMapping("/get")
    public List<USPopulation> get() {
         return populationDao.queryAll();
    }
}
