package com.xiaojie.hadoop.entity;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 商品明细实体类
 * @date 2025/1/7 14:57
 */
@Data
@Builder
public class Commodity {

    //商品id
    private Long id;

    //分类id
    private Long categoryId;

    //商品名称
    private String name;

    //价格
    private BigDecimal price;

    //商品图片
    private String picture;

    //描述
    private String description;

}
