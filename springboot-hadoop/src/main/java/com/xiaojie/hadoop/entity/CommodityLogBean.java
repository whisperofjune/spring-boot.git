package com.xiaojie.hadoop.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 商品明细实体类
 * @date 2025/1/7 14:57
 */
@Data
public class CommodityLogBean {

    //行键

    @TableField(value = "row_id")
    private String  rowId;

    //商品id
    @TableField(value = "commodity_id")
    private String commodityId;

    //分类id
    @TableField(value = "category_id")
    private String categoryId;

    //商品名称
    private String name;

    //价格
    private String price;

    //商品图片
    private String picture;

    //用户id
    @TableField(value = "user_id")
    private String userId;

    //点击时间
    @TableField("click_time")
    private String clickTime;

}
