package com.xiaojie.aspect;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/6/11 20:54
 */
public interface DataAspectService {

    Object userAspect(Object _obj,String value, String pin,String scope, String shopName, String shopCode);
}
