package com.xiaojie.aspect;

import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/6/11 20:59
 */
@Component
public class DataAspectServiceImpl  implements DataAspectService{
    @Override
    public Object userAspect(Object _obj, String value, String name, String scope, String shopName, String shopCode) {
        if("1".equals(scope)){//本人
            _obj=changObjectValue(_obj,"name",name);
        }else if("2".equals(scope)){//本人及下属
            _obj=changObjectValue(_obj,"ids",new ArrayList<String>());
        }else if("3".equals(scope)){//本店
            _obj=changObjectValue(_obj,"shopCode",shopCode);
        }
        return _obj;
    }

    private   Object changObjectValue(Object _obj, String name,Object value) {
        try {
            Class<?> resultClz = _obj.getClass();
            //获取class里的所有字段  父类字段获取不到
            Field[] fieldInfo = resultClz.getDeclaredFields();
            for (Field field : fieldInfo) {
                if(name.equals(field.getName())){
                    //成员变量为private,故必须进行此操
                    field.setAccessible(true);
                    field.set(_obj, value);
                }
            }
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return _obj;
    }
}
