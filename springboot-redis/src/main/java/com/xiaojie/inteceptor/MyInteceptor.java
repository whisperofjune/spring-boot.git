package com.xiaojie.inteceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 拦截器
 * @date 2021/12/14 23:51
 */
@Component
public class MyInteceptor  implements HandlerInterceptor {

    /**
     * @description: 方法执行之前
     * @author xiaojie
     * @date 2021/12/14 23:53
     * @version 1.0
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return HandlerInterceptor.super.preHandle(request, response, handler);

    }

    /**
     * @description: 方法执行时
     * @author xiaojie
     * @date 2021/12/14 23:54
     * @version 1.0
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    /**
     * @description: 方法执行之后
     * @author xiaojie
     * @date 2021/12/14 23:54
     * @version 1.0
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
