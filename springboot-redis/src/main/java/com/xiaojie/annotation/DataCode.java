package com.xiaojie.annotation;

import java.lang.annotation.*;

/**
 * @author xiaojie
 * @description: 数据权限自定义权限码
 * @param:
 * @return:
 * @date: 2022/6/11 20:19
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataCode {
    enum Type {
        USER("user"), //用户类型
        SHOP("shop");  //店铺类型      ;
        String value;

        Type(String value) {
            this.value = value;
        }
    }

    Type type();//类型

    String value() default ""; //内容

    String name() default ""; //name

    String scope() default ""; //权限范围值

    String shopName() default ""; //门店名称

    String shopCode() default "";//门店编码
}
