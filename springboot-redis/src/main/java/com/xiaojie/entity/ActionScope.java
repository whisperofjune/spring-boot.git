package com.xiaojie.entity;

import lombok.Data;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 权限封装类
 * @date 2022/6/11 20:33
 */
@Data
public class ActionScope {
    private Integer scope;

    private String shopCode;

    private String ShopName;

    private String name;

}
