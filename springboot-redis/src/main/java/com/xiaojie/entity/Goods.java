package com.xiaojie.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 商品
 * @date 2022/6/9 22:56
 */
@Data
@ApiModel(value = "Goods",description = "商品实体类")
public class Goods {
    @ApiModelProperty(value = "id",required = true)
    private Long id; //id
    @ApiModelProperty(value = "name",required = true)
    private String name;//名称
    @ApiModelProperty(value = "stock",required = true)
    private Long stock;//库存
    @ApiModelProperty(value = "price",required = true)
    private BigDecimal price;//价格
}
