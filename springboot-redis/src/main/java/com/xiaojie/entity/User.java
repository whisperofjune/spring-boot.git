package com.xiaojie.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.09.08
 */
@Data
@ApiModel(value = "User",description = "用户实体类")
public class User implements Serializable {
    @ApiModelProperty(value = "id",required = true)
    private Integer id;
    @ApiModelProperty(value = "name",required = true)
    private String name;
    @ApiModelProperty(value = "age",required = true)
    private Integer age;
    @ApiModelProperty(value = "num",required = true)
    private Integer num;
}
