package com.xiaojie.rocket.producer;

import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.GeoRadiusParam;

import java.util.List;

/**
 * @Description:Redis
 * @author: xiaojie
 * @date: 2021.09.14
 */
public class RedisGeoTest {
    /*
     * @param null
     * @geoadd：添加地理位置的坐标。
        geopos：获取地理位置的坐标。
        geodist：计算两个位置之间的距离。
        georadius：根据用户给定的经纬度坐标来获取指定范围内的地理位置集合。
        georadiusbymember：根据储存在位置集合里面的某个地点获取指定范围内的地理位置集合。
        geohash：返回一个或多个位置对象的 geohash 值。
        * 有效的经度从-180度到180度。longitude
        *有效的纬度从-85.05112878度到85.05112878度。latitude
     * @author xiaojie
     * @date 2021/9/14
     * @return
     */
    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.168.6.137", 6379);
        jedis.geoadd("dist", 116.25, 39.54, "bj");
        jedis.geoadd("dist", 116.45, 38.34, "tj");
        jedis.geoadd("dist", 114.30, 37.27, "sjz");
        //获取经纬度
        List<GeoCoordinate> geopos = jedis.geopos("dist", "tj", "bj");
        for (int i = 0; i < geopos.size(); i++) {
            System.out.println("经度是：" + geopos.get(i).getLongitude() + "纬度是：" + geopos.get(i).getLatitude());
        }
        //计算两地之间的距离 默认单位是米
        System.out.println("北京和天津的距离是：" + jedis.geodist("dist", "bj", "tj", GeoUnit.KM) + "km");
        //根据给定的坐标来获取指定范围内的地理位置集合,按照距离正序排列
        List<GeoRadiusResponse> dist = jedis.georadius("dist", 115.88, 37.30, 150, GeoUnit.KM, GeoRadiusParam.geoRadiusParam().withDist().sortAscending());
        for (int i = 0; i < dist.size(); i++) {
            System.out.println("距离给定地点的位置是：" + dist.get(i).getMemberByString() + "相距:" + dist.get(i).getDistance() + "km");
        }
        //查询距北京150km范围内的地点
        List<GeoRadiusResponse> bj150 = jedis.georadiusByMember("dist", "bj", 150, GeoUnit.KM, GeoRadiusParam.geoRadiusParam().withDist().sortAscending());
        for (int i = 0; i < bj150.size(); i++) {
            System.out.println("距离北京150km内位置是：" + bj150.get(i).getMemberByString() + "相距:" + bj150.get(i).getDistance() + "km");
        }
    }
}
