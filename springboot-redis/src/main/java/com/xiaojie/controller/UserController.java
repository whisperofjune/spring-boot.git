package com.xiaojie.controller;

import com.xiaojie.entity.ActionScope;
import com.xiaojie.entity.User;
import com.xiaojie.entity.UserQuery;
import com.xiaojie.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.09.08
 */
@RestController
@Api(tags = "测试用户接口")
public class UserController {



    @Autowired
    private UserService userService;
    @GetMapping("/getByName")
    @ApiOperation(value="根据用户名称查询用户接口【1】")
    public User getByName(String name){
        return userService.getUserByName(name);
    }


    @RequestMapping("/getListUsers")
    public List<User> getListUsers() {
        return userService.getUSerList();
    }

    /**
     * 提前将数据添加到布隆过滤器
     * @return
     */
    @RequestMapping("/preBlongData")
    public String preBlongData(){
        userService.preBlongData();
        return "success";
    }

    @RequestMapping("/list")
    public List<User>  list(Integer page,Integer pageSize){
        ActionScope scope=new ActionScope();
        scope.setScope(3);
        scope.setName("admin");
        scope.setShopCode("tj");
        UserQuery query=new UserQuery();
        query.setStart(page);
        query.setEnd(pageSize);
       return  userService.list(query,scope);
    }

}
