package com.xiaojie.controller;

import com.xiaojie.entity.Goods;
import com.xiaojie.lock.RedisLock;
import com.xiaojie.mapper.GoodsMapper;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 秒杀的接口
 * @date 2022/6/9 22:55
 */
@RestController
public class RedissonSeckillController {
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private RedissonClient redissonClient;

    @GetMapping("/secKillRedisson")
    public String secKillRedisson(Long id) {
        RLock rLock = null;
        try {
            rLock = redissonClient.getLock(id + "");
            rLock.lock(); //加锁，加几次锁，finally释放锁的时候就要释放几次锁
            rLock.lock();
            Goods goods = goodsMapper.selectById(id);
            if (null == goods) {
                return "该商品没有秒杀活动";
            }
            if (goods.getStock() <= 0) {
                return "商品库存为空了。。。。。";
            }
            //减库存
            Integer result = goodsMapper.deceGoods(id);
            return result > 0 ? "秒杀成功" : "秒杀失败";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return "fail";
    }
}
