package com.xiaojie.lock;


import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 记录锁信息
 * @date 2022/6/9 17:44
 */
public class RedisLockInfo {
    /**
     * 锁的状态 state为true 则表示获取锁成功
     */
    private boolean state;
    /**
     * 锁的id
     */
    private String lockId;
    /**
     * 锁的持有线程
     */
    private Thread lockThread;

    /**
     * 锁的过期时间
     */
    private Long expire;

    /**
     * 续命次数
     */
    private AtomicInteger lifeCount;

    /**
     * 获取锁次数
     */
    private AtomicInteger lockCount;

    // 锁的可重入次数
    public RedisLockInfo(String lockId, Thread lockThread, Long expire) {
        state = true;
        this.lockId = lockId;
        this.lockThread = lockThread;
        this.expire = expire;
        lifeCount = new AtomicInteger(0);
        lockCount = new AtomicInteger(0);
    }

    public RedisLockInfo(Thread lockThread, Long expire) {
        this.lockThread = lockThread;
        this.expire = expire;
        lifeCount = new AtomicInteger(0);
        lockCount = new AtomicInteger(0);
        state = true;
    }

    public RedisLockInfo() {
        state = true;
    }

    public String getLockId() {
        return lockId;
    }

    public boolean isState() {
        return state;
    }

    public Thread getLockThread() {
        return lockThread;
    }

    public Long getExpire() {
        return expire;
    }

    //重试的次数
    public Integer getLifeCount() {
        return lifeCount.incrementAndGet();
    }

    //锁获取的次数
    public Integer incrLockCount() {
        return lockCount.incrementAndGet();
    }

    //释放锁次数
    public Integer decreLockCount() {
        return lockCount.decrementAndGet();
    }
}
