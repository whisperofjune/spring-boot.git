package com.xiaojie.lock;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 分布式锁的lock
 * @date 2022/6/9 18:03
 */
public interface RedisLock {
    //获取锁
    boolean tryLock();
    //释放锁
    boolean releaseLock();
}
