package com.xiaojie.mapper;

import com.xiaojie.annotation.SelectProvider;
import com.xiaojie.cache.MybatisRedisCache;
import com.xiaojie.entity.User;
import com.xiaojie.entity.UserQuery;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.09.08
 */
@CacheNamespace(implementation= MybatisRedisCache.class)
public interface UserMapper {
    @Delete("delete from tb_user where name = #{name}")
    int deleteByName(String name);

    @Insert("insert into tb_user values(#{id}, #{name},#{age})")
    int insert(User record);

    @Select("select id,name,age from tb_user where name = #{name}")
    User selectByName(String name);

    @Update("update tb_user set name = #{name} where id = #{id}")
    int update(User record);
    @Select("select id,name,age,num from tb_user ")
    List<User> selectAll();

    @Update("update tb_user set num = #{num} where id = #{id}")
    int updateNum(Integer num,Integer id);

//    @Select("select id,name,age,num from tb_user where name=#{name} and shopCode=#{shopCode} limit #{start},#{end} ")
    @Select("<script>" +
            " select id,name,age,num from tb_user where 1=1" +
            " <if test='name != null and name != \"\"'> "+
            "  and name=#{name} " +
            " </if> " +
            " <if test='shopCode != null and shopCode != \"\"'> "+
            "  and shopCode=#{shopCode} " +
            " </if> " +
            "</script>")
    List<User> list(UserQuery query);
}
