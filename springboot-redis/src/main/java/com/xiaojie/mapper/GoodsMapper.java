package com.xiaojie.mapper;

import com.xiaojie.entity.Goods;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/6/9 22:58
 */
public interface GoodsMapper {

    @Select("select * from tb_goods where id = #{id}")
    Goods selectById(Long id);

    @Update("update tb_goods set stock=stock-1 where id = #{id}")
    Integer deceGoods(Long id);
}
