package com.xiaojie.redisson.redlock;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 红锁配置设置集群模式
 * 解决当主节点还没有把数据同步到从节点，从节点宕机时的情况
 * @date 2022/4/1 22:42
 */
//@Configuration
public class ReddissonConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password}")
    private String password;

    @Bean("redissonClient1")
    @Primary
    public RedissonClient redissonClient1() {
        Config config = new Config();
        //设置看门狗时间
//        config.setLockWatchdogTimeout(30000L);
        //设置单机版本redis
        config.useSingleServer().setAddress("redis://" + host + ":" + 6379);
        //设置密码
        config.useSingleServer().setPassword(password);
        return Redisson.create(config);
    }

    @Bean("redissonClient2")
    public RedissonClient redissonClient2() {
        Config config = new Config();
        //设置看门狗时间
//        config.setLockWatchdogTimeout(30000L);
        //设置单机版本redis
        config.useSingleServer().setAddress("redis://" + host + ":" + 6380);
        //设置密码
        config.useSingleServer().setPassword(password);
        return Redisson.create(config);
    }

    @Bean("redissonClient3")
    public RedissonClient redissonClient3() {
        Config config = new Config();
        //设置看门狗时间
//        config.setLockWatchdogTimeout(30000L);
        //设置单机版本redis
        config.useSingleServer().setAddress("redis://" + host + ":" + 6380);
        //设置密码
        config.useSingleServer().setPassword(password);
        return Redisson.create(config);
    }
}
