package com.xiaojie.rocket.rocket.controller;

import com.xiaojie.rocket.rocket.producer.MqProducer;
import com.xiaojie.rocket.rocket.producer.OrderProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: controller
 * @author: yan
 * @date: 2021.11.08
 */
@RestController
public class MqController{
    @Autowired
    private MqProducer mqProducer;
    @Autowired
    private OrderProducer orderProducer;

    @GetMapping("/send")
    public String send(){
        mqProducer.sendMq();
        return "success";
    }

    @GetMapping("/sync")
    public String sync(){
        mqProducer.sync();
        return "success";
    }
    @GetMapping("/syncOrder")
    public String syncOrder(){
        mqProducer.syncOrder();
        return "success";
    }
    @GetMapping("/async")
    public String async(){
        mqProducer.async();
        return "success";
    }

    @GetMapping("/orderSend")
    public String orderSend(){
        orderProducer.orderSend();
        return "success";
    }
}
