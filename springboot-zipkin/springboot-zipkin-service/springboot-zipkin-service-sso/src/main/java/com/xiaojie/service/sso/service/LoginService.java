package com.xiaojie.service.sso.service;

import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 登录的接口
 * @date 2022/4/29 17:19
 */
public interface LoginService {

    /**
     * @description: 登录接口
     * @author xiaojie
     * @date 2022/4/29 17:21
     * @version 1.0
     */
    @GetMapping("/login")
     String login(@Param(value="name") String name,@Param(value="pwd") String pwd);
}
