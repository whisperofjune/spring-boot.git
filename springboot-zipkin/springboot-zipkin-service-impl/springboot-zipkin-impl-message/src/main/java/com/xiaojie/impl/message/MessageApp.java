package com.xiaojie.impl.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.zipkin2.ZipkinRestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/4/29 17:56
 */
@SpringBootApplication
public class MessageApp {

    public static void main(String[] args) {
        SpringApplication.run(MessageApp.class);
    }
}
