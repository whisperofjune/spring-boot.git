package com.xiaojie.impl.sso.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/5/2 16:17
 */
@TableName("tb_user")
@Data
public class User {

    @TableId
    private Integer id ;
    private String name;
    private Integer age;
    private String position;
    private String address;
    private Date createTime;
    private Date updateTime;
    private int deleteFlag;
    private String pwd;
}
