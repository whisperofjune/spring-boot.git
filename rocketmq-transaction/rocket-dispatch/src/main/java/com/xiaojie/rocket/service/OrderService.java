package com.xiaojie.rocket.service;

import com.xiaojie.rocket.pojo.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【tb_order】的数据库操作Service
* @createDate 2021-11-14 22:49:21
*/
public interface OrderService extends IService<Order> {

}
