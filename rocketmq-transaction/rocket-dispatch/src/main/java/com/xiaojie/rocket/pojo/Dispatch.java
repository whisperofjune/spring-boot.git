package com.xiaojie.rocket.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName tb_dispatch
 */
@TableName(value ="tb_dispatch")
@Data
public class Dispatch implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    private String orderid;

    /**
     * 
     */
    private Long userid;

    /**
     * 
     */
    private Long riderid;

    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer status;

    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createtime;

    /**
     * 
     */
    private Long sendtime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}