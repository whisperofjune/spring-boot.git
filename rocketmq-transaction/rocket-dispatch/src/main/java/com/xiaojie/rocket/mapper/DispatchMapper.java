package com.xiaojie.rocket.mapper;

import com.xiaojie.rocket.pojo.Dispatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【tb_dispatch】的数据库操作Mapper
* @createDate 2021-11-14 22:48:30
* @Entity com.xiaojie.rocket.pojo.Dispatch
*/
public interface DispatchMapper extends BaseMapper<Dispatch> {

}




