package com.xiaojie.rocket.service;

import com.xiaojie.rocket.pojo.Dispatch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Administrator
* @description 针对表【tb_dispatch】的数据库操作Service
* @createDate 2021-11-14 22:48:30
*/
public interface DispatchService extends IService<Dispatch> {

}
