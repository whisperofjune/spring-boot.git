package com.xiaojie.rocket.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaojie.rocket.pojo.Order;
import com.xiaojie.rocket.service.OrderService;
import com.xiaojie.rocket.mapper.OrderMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【tb_order】的数据库操作Service实现
* @createDate 2021-11-14 22:49:21
*/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
    implements OrderService{

}




