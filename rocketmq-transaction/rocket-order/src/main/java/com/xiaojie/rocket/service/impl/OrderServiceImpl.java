package com.xiaojie.rocket.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaojie.rocket.pojo.Order;
import com.xiaojie.rocket.producer.OrderProducer;
import com.xiaojie.rocket.service.OrderService;
import com.xiaojie.rocket.mapper.OrderMapper;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
* @author Administrator
* @description 针对表【tb_order】的数据库操作Service实现
* @createDate 2021-11-14 22:45:05
*/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
    implements OrderService{
    @Autowired
    private OrderProducer orderProducer;
    @Autowired
    private OrderMapper orderMapper;

    private String ORDER_TOPIC_TEST="order-topic-test";

    @Override
    public TransactionSendResult saveOrder() {
        Order order = new Order();
        String orderId = UUID.randomUUID().toString();
        order.setOrderid(orderId);
        order.setOrdername("小谷姐姐麻辣烫");
        order.setPaymoney(35.68);
        order.setStatus(1);//假设订单支付完成
        String jsonString = JSONObject.toJSONString(order);
         return orderProducer.sendSyncMessage(jsonString,ORDER_TOPIC_TEST,null);
    }


}




