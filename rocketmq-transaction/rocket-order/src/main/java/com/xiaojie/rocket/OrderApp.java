package com.xiaojie.rocket;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/11/15 0:32
 */
@SpringBootApplication
@MapperScan("com.xiaojie.rocket.mapper")
public class OrderApp {
    public static void main(String[] args) {
        SpringApplication.run(OrderApp.class);
    }
}
