package com.xiaojie.rocket.service;

import com.xiaojie.rocket.pojo.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.rocketmq.client.producer.TransactionSendResult;

/**
* @author Administrator
* @description 针对表【tb_order】的数据库操作Service
* @createDate 2021-11-14 22:45:05
*/
public interface OrderService extends IService<Order> {

     TransactionSendResult saveOrder();
}
