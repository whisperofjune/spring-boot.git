package com.xiaojie.rocket.controller;

import com.xiaojie.rocket.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/11/15 0:36
 */
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/saveOrder")
    private Object saveOrder(){
        return orderService.saveOrder();
    }
}
