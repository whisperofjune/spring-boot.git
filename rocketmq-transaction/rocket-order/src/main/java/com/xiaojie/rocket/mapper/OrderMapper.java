package com.xiaojie.rocket.mapper;

import com.xiaojie.rocket.pojo.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Administrator
* @description 针对表【tb_order】的数据库操作Mapper
* @createDate 2021-11-14 22:45:05
* @Entity com.xiaojie.rocket.domain.Order
*/
public interface OrderMapper extends BaseMapper<Order> {

}




