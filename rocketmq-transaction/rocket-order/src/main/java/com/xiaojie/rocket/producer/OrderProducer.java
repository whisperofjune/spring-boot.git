package com.xiaojie.rocket.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 订单生产者
 * @date 2021/11/14 23:25
 */
@Component
@Slf4j
public class OrderProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    public TransactionSendResult sendSyncMessage(String msg, String destination, String tag) {
        log.info("【发送消息】：{}........", msg);
        Message<String> message = MessageBuilder.withPayload(msg).build();
        TransactionSendResult result = rocketMQTemplate.sendMessageInTransaction(destination,  message,null);
        log.info("【发送状态】：{}", result.getLocalTransactionState());
        return result;
    }
}
