package com.xiaojie.controller;

import com.xiaojie.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.10.14
 */
@RestController
public class MsgController {

    @Autowired
    private KafkaProducer kafkaProducer;

    @GetMapping("/kafka")
    public String kafka(){
//         kafkaProducer.sendMSg();
        kafkaProducer.sendMSg2();
         return "success";
    }

    @GetMapping("/callBack")
    public String callBack(){
        String msg="callback--------------------";
//        kafkaProducer.sendMsgCallback(msg);
        kafkaProducer.sendMsgCallback1(msg);
        return "success";
    }

    @GetMapping("/txKafka")
    public String txKafka(){
        kafkaProducer.sendTx();
        return "success";
    }

    @GetMapping("/testXiaojie")
    public String testXiaojie(){
        kafkaProducer.sendXiaojieMSg();
        kafkaProducer.sendTestMSg();
        return "success";
    }

    @GetMapping("/filterKafka")
    public String filterKafka(String msg){
        kafkaProducer.sendFilterMsg(msg);
        return "success";
    }
}
