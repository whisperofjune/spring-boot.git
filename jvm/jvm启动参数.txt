-Xmx1G
-Xms1G
-Xmn256m
-XX:SurvivorRatio=8
-XX:+UseG1GC
-XX:MaxTenuringThreshold=15
-XX:ParallelGCThreads=4
-XX:ConcGCThreads=4
-XX:+DisableExplicitGC
-XX:+HeapDumpOnOutOfMemoryError
-XX:HeapDumpPath=d:/a.dump
-XX:+PrintGCDetails
-XX:+PrintClassHistogram
--add-opens=java.base/java.lang=ALL-UNNAMED


#设置远程连接时权限问题，适用jdk9之前版本
grant codebase "file:${java.home}/../lib/tools.jar" {
    permission java.security.AllPermission;
};

jstatd -J-Djava.security.policy=/root/test/jstatd.policy